const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');

const indexRouter = require('./routes/index');
const mouvementsRouter = require('./routes/movements');
const analyzeRouter = require('./routes/analyze');
const categoriesRouter = require('./routes/categories');

const app = express();

// view engine setup
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'hbs');

// Serve static files from the React app
app.use(express.static(path.join(__dirname, 'client/build/index.html')));


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(
    cors({
        origin: '*',
        methods: 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
    })
);

//app.use('/', indexRouter);
app.use('/movements', mouvementsRouter);
app.use('/analyze', analyzeRouter);
app.use('/categories', categoriesRouter);

// The "catchall" handler: for any request that doesn't
// match one above, send back React's index.html file.
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname+'/client/build/index.html'));
});

// catch 404 and forward to error handler
//app.use(function(req, res, next) {
//    next(createError(404));
//});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
